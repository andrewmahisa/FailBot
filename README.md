Required Components 
-------------------


```
*Tensorflow r11.0 with or without GPU Support (Version 1.0 is not yet supported)
*Python >=2.7.0 

```



How to Train
------------------


The program takes any kind of plain text file as an input, call 'python train.py --data_dir=./data/sample/' to train the neural net using the data in the directory that's passed as an argument.



How to use the chatbot
------------------

Train the chatbot yourself, or use my pre-trained models that's trained using reddit's comment for 72 hours ( 3 days ) on my GTX750Ti GPU, extract the folder to the '/models' directory

run 'python chatbot.py' and try to have an conversation with the chatbot.




Credits
--------------

This project will not be possible without:

* sherjilozair's port of LSTN Recurrent Neural Net using tensorflow : https://github.com/sherjilozair/char-rnn-tensorflow

* python reddit API : https://praw.readthedocs.io
